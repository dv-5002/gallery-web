import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { AuthProvider } from "./Context"
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPlus, faImages, faUpload } from "@fortawesome/free-solid-svg-icons"
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { StylesProvider } from "@material-ui/core/styles"
import CssBaseline from "@material-ui/core/CssBaseline"

library.add(faPlus, faImages, faUpload)

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#ec6424"
    },
    secondary: {
      main: "#000000"
    },
    type:'dark'
  }
})
console.log(theme)

ReactDOM.render(
  <React.StrictMode>
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <StylesProvider injectFirst>
        <AuthProvider>
          <App />
        </AuthProvider>
      </StylesProvider>
    </MuiThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
