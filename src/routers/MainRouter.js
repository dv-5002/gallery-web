import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";

import Navbar from "../components/Navbar";
import { useAuthState } from "../Context";
import Signin from "../screens/Signin";
import Signup from "../screens/Signup";
import Home from "../screens/Home";
import MyGallery from "../screens/MyGallery";
import Profile from "../screens/Profile";

function Private({ children, ...rest }) {
    let user = useAuthState();
    return (
        <Route
            {...rest}
            render={({ location }) =>
                user.isLogin ? (
                    children
                ) : (
                        <Redirect to={{ pathname: "/signin", state: { from: location } }} />
                    )
            }
        ></Route>
    );
}

function MainRouter() {
    return (
            <Router>
                <Navbar />
                <Switch>
                    <Route exact path="/signin">
                        <Signin />
                    </Route>
                    <Route exact path="/signup">
                        <Signup />
                    </Route>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Private path="/gallery/my">
                        <MyGallery />
                    </Private>
                    <Private path="/profile">
                        <Profile />
                    </Private>
                    <Route path="*">
                        <Redirect to={{ pathname: "/" }} />
                    </Route>
                </Switch>
            </Router>
    );
}

export default MainRouter;
