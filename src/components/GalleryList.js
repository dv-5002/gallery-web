import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link, useRouteMatch } from "react-router-dom";
import API from "../api";
import { Container, Grid, CardMedia, CardContent, Typography, Button, Card, CardActions, IconButton, Box, CardActionArea, Switch, FormControlLabel } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import DeleteIcon from '@material-ui/icons/Delete';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';

export default function () {
  const { url } = useRouteMatch();
  const [galleries, setGalleries] = useState([]);
  const [checked, setChecked] = useState(false);
  const classes = useStyles()

  useEffect(() => {
    API.listGallery()
      .then((res) => {
        setGalleries(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  useEffect(() => {
    API.listGalleryImage()
      .then((res) => setGalleries(res.data))
      .catch((err) => console.error(err));
  })

  const handleDelete = (id) => {
    API.deleteGallery(id)
      .then(() => {
        const filter = galleries.filter((g) => g.id !== id);
        setGalleries(filter);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const updateStatus = ({ id, is_publish }) => {
    API.updateGalleryStatus({ id, is_publish: !is_publish })
      .then(() => {
        const update = galleries.map((gal) => {
          if (gal.id === id) {
            return { ...gal, is_publish: !is_publish };
          }
          return gal;
        });
        setGalleries(update);
      })
      .catch(() => {});
  };

  return (
    <Container maxWidth="md">
      <Grid container spacing={4}>
        <Grid container
          justify="center"
          alignItems="flex-end" >
          <Box mt={8}>
            <Card
              style={{ border: "5px dashed #ec6424", borderRadius: 100 }}
              onClick={() => window.location.pathname = `${url}/gallery/new`}>
              <CardActionArea>
                <CardContent>
                  <Typography
                    color='primary'
                    style={{
                      fontSize: 24,
                      fontWeight: "bold",
                      paddingTop: 65,
                      paddingBottom: 65,
                    }}>
                    <AddPhotoAlternateIcon color='text' />
                  New Gallary
                </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Box>
        </Grid>
        {galleries.map((gallery) => (
          <Grid item key={gallery.id} sm={12} md={4}>
            <Box mt={8}>
              <Card className={classes.card}>
                <CardActionArea onClick={() => window.location.pathname = `${url}/gallery/${gallery.id}`} >
                  <CardMedia
                    className={classes.cardMedia}
                    image={"https://images.unsplash.com/photo-1514195037031-83d60ed3b448?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80"} />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {gallery.name}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={gallery.is_publish ? true : false}
                        color="primary"
                      />
                    }
                    onClick={() => updateStatus(gallery)}
                    label={gallery.is_publish ? "Public" : "Private"}
                  />
                  <Grid
                    container
                    justify="flex-end"
                    alignItems="flex-end">
                    <IconButton size="small" color="primary" onClick={() => handleDelete(gallery.id)}>
                      <DeleteIcon />
                    </IconButton>
                  </Grid>
                </CardActions>
              </Card>
            </Box>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const styles = {
  container: {
    flex: 1,
    padding: "24px 56px",
  },
  addCard: {
    border: "2px dashed black",
    cursor: "pointer",
  },
  addedCard: {
    // height: 240,
  },
  gallaryText: { textAlign: "center", fontSize: 24, fontWeight: "bold" },
  normalText: {
    textAlign: "center",
    fontSize: 24,
    fontWeight: "bold",
    paddingTop: 200,
    // paddingBottom: 100,
    minHeight: 230,
    opacity: 0.6,
    marginBottom: 48,
    transition: "ease 0.3s",
  },
  hoverText: {
    textAlign: "center",
    fontSize: 24,
    fontWeight: "bold",
    paddingTop: 200,
    // paddingBottom: 100,
    minHeight: 230,
    backgroundColor: "#eee",
    marginBottom: 48,
    borderRadius: 12,
    color: "#333",
    transition: "ease 0.3s",
  },
  uploadImg: {
    marginBottom: 48,
    width: "100%",
    padding: 0,
  },
  button: {
    width: "100%",
  },
};