import React, { useEffect, useState } from "react";
import { Link, useHistory, useParams, useRouteMatch } from "react-router-dom";
import { useAuthState, useAuthDispatch } from "../Context";
import API from "../api";
import Storage from "../storage";
import { AppBar, Typography, Button, IconButton, Switch, Toolbar, Menu, MenuItem } from '@material-ui/core'
import AccountCircle from '@material-ui/icons/AccountCircle'
import { makeStyles } from '@material-ui/core/styles'

export default function () {
    const { url } = useRouteMatch();
    const user = useAuthState();
    const dispatch = useAuthDispatch();
    const history = useHistory();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const { id } = useParams();
    const [users, setUsers] = useState();

    const logout = () => {
        API.logout()
            .then(() => {
                Storage.saveToken("");
                dispatch({ type: "SET_USER", isLogin: false });
                history.replace("/");
            })
            .catch((err) => {
                console.error(err);
            });
    };

    // useEffect(() => {
    //     if (currrentID !== null) {
    //         const userID = parseInt(currrentID);
    //         API.getUser(userID)
    //             .then((res) => {
    //                 const { ID, Firstname, Lastname } = res.data;
    //                 setUser({ ID, Firstname, Lastname });
    //             })
    //             .catch((err) => console.error(err));
    //     }
    // }, [id]);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    console.log(user)

    return (
        <div>
            <AppBar position="static" color="secondary">
                <Toolbar>
                    {user.isLogin ? (
                        <div style={{ flexGrow: 1, display: 'flex' }}>
                            <img
                                src={require("../assets/logo.png")}
                                onClick={() => window.location.pathname = "/"}
                                style={{ wigth: 50, height: 50 }} />
                        </div>
                    ) : (
                            <div style={{ flexGrow: 1, display: 'flex' }}>
                                <img
                                    src={require("../assets/logo.png")}
                                    onClick={() => window.location.pathname = "/"}
                                    style={{ wigth: 50, height: 50 }} />
                            </div>
                        )}
                    <div>
                        {user.isLogin && (
                            <div>
                                <IconButton
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    color="inherit"
                                >
                                    <AccountCircle />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={handleClose}
                                >
                                    <MenuItem onClick={() => window.location.pathname = "/gallery/my"}>My Gallery</MenuItem>
                                    <MenuItem onClick={() => window.location.pathname = `/profile`}>Edit Profile</MenuItem>
                                    <MenuItem onClick={() => logout()}>Logout</MenuItem>
                                </Menu>
                            </div>
                        )}
                        {!user.isLogin && (
                            <>
                                <Button
                                    color="primary"
                                    variant={'outlined'}
                                    onClick={() => window.location.pathname = "/signin"}>
                                    Login
                                </Button>
                            </>
                        )}
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
}
