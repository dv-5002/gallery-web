import React from 'react';
import logo from './logo.svg';
import './App.css';
import Gallery from './screens/Gallery';
import MainRouter from './routers/MainRouter';

function App() {
  return (
    <div className="App">
      <MainRouter />
    </div>
  );
}

export default App;
