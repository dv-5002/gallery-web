import React, { useState, useEffect } from "react";
import API from "../api";
import { Typography, Grid, Card, CardActionArea, CardContent, CardMedia, Hidden, Container, GridList, GridListTile } from '@material-ui/core';
import Gallery from "react-photo-gallery"
import { useParams } from "react-router";
import { makeStyles } from '@material-ui/core/styles'

export default function Home() {
    const [galleries, setGalleries] = useState([]);
    const classes = useStyles()

    useEffect(() => {
        API.listPublishGallery()
            .then((res) => setGalleries(res.data))
            .catch((err) => console.error(err));
    }, []);

    return (
        <Container style={{ marginTop: 16 }}>
            <Grid >
                {galleries.map((gal) => (
                    <Grid style={{marginBottom:16}}>
                        <Typography component="h2" variant="h5" style={{backgroundColor:"#3C3C3C"}}>
                            {gal.name}
                        </Typography>
                        <GridList cellHeight={250} className={classes.gridList} cols={3}>
                            {gal.images.map((img) => (
                                <GridListTile key={img.id}>
                                    <img
                                        className={classes.image}
                                        src={`${API.host}/${img.filename}`} />
                                </GridListTile>
                            ))}
                        </GridList>
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-around",
        overflow: "hidden",
        marginBottom: 32,
    },
    gridList: {
        //width: 900,
        height: "auto",
    },
    icon: {
        color: "rgba(255, 255, 255, 0.54)"
    },
    image: {
        "&:hover": {
            opacity: 0.25
        }
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
            'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    }
}))