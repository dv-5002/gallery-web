import React, { useState } from "react";
import { useHistory, Route, Redirect } from "react-router-dom";
import API from "../api";
import { useAuthState, useAuthDispatch } from "../Context";
import Storage from "../storage";
import { Typography, Grid, Container, Paper, Link, Card, FormControlLabel, TextField, CssBaseline, Button, Avatar } from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/core/styles'
import Image from '../assets/image.jpg'
import styled from "styled-components"

export default function Signin() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const history = useHistory();
  const dispatch = useAuthDispatch();
  const user = useAuthState();
  const classes = useStyles()

  const handleSubmit = (e) => {
    e.preventDefault();
    API.login({ email, password })
      .then((res) => {
        dispatch({ type: "SET_USER", isLogin: true, email });
        console.log(res)
        Storage.saveToken(res.data.token);
        // history.replace("/admin");
      })
      .catch(() => {
        setMessage("Login fail! Username or Password is invalid");
      });
  };

  return (
    <Route
      render={() =>
        user.isLogin ? (
          <Redirect to="/gallery/my" />
        ) : (
            <>
              <ImgBackground style={styles.paperContainer} />
              <Container maxWidth="xs">
                <Card className={classes.paper}>
                  <img
                    src={require("../assets/logo.png")}
                    style={{ wigth: 50, height: 50 }} />
                  <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                  <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      id="email"
                      label="Email Address"
                      name="email"
                      type="email"
                      autoComplete="email"
                      autoFocus
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      onFocus={() => setMessage("")} />
                    <TextField
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      name="password"
                      label="Password"
                      type="password"
                      id="password"
                      autoComplete="current-password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                    />
                    {message && <div className="message message--error">{message}</div>}
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}>
                      Sign In
                  </Button>
                    <Grid container justify="flex-end">
                      <Grid item>
                        <Link onClick={() => window.location.pathname = "/signup"} variant="body2">
                          {"Don't have an account? Sign Up"}
                        </Link>
                      </Grid>
                    </Grid>
                  </form>
                </Card>
              </Container>
            </>
          )
      }
    ></Route>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 50,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const styles = {
  paperContainer: {
    backgroundImage: `url(${Image})`,
    backgroundSize: "cover",
    position: 'absolute',
    width: '100%',
    height: '91%',
  }
};

const ImgBackground = styled(Paper)`
  z-index: -1;
  filter: blur(15px) brightness(0.7)
`
