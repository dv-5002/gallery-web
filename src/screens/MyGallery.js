import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import Gallery from "./Gallery";
import GalleryList from "../components/GalleryList";
import Profile from "./Profile";

export default function MyGallery() {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route exact path={path}>
        <GalleryList />
      </Route>
      <Route path={`${path}/gallery/:id`}>
        <Gallery />
      </Route>
    </Switch>
  );
}
