import React, { useState } from "react";
import { useHistory, Route, Redirect } from "react-router-dom";
import { useAuthState, useAuthDispatch } from "../Context";
import API from "../api";
import Storage from "../storage";
import { Avatar, Button, Card, TextField, Paper, Checkbox, Link, Grid, Box, Typography, Container, Input } from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/core/styles'
import styled from "styled-components"
import Image from '../assets/image2.jpg'
import AddIcon from "@material-ui/icons/Add";

export default function Signup() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [profileImg, setProfileImg] = useState("");
    const [message, setMessage] = useState("");
    const history = useHistory();
    const dispatch = useAuthDispatch();
    const user = useAuthState();
    const classes = useStyles();

    const handleSubmit = (e) => {
        e.preventDefault();
        if (email.trim() === "" || password.trim() === "") {
            setMessage("Email or Password can't be empty");
            return;
        }
        if (password.trim() !== passwordConfirm.trim()) {
            setMessage("Password and Confirm is not match");
            return;
        }
        if (firstname.trim() === "") {
            setMessage("Firstname can't be empty");
            return;
        }
        if (lastname.trim() === "") {
            setMessage("Lastname can't be empty");
            return;
        }
        API.signup({ email, password, firstname, lastname })
            .then((res) => {
                const { email, token } = res.data;
                dispatch({ type: "SET_USER", isLogin: true, email, firstname, lastname });
                Storage.saveToken(token);
                history.replace("/admin");
            })
            .catch(() => {
                setMessage("Login fail! Username or Password is invalid");
            });
    };

    console.log(profileImg)

    return (
        <Route
            render={() =>
                user.isLogin ? (
                    <Redirect to="/gallery/my" />
                ) : (
                        <>
                            <ImgBackground style={styles.paperContainer} />
                            <Container component="main" maxWidth="xs">
                                <Card className={classes.paper}>
                                    <img
                                        src={require("../assets/logo.png")}
                                        style={{ wigth: 50, height: 50 }} />
                                    <Typography component="h1" variant="h5">
                                        Sign up
                                </Typography>
                                    <form className={classes.form} onSubmit={handleSubmit}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={12} sm={6}>
                                                <TextField
                                                    autoComplete="fname"
                                                    name="firstName"
                                                    variant="outlined"
                                                    required
                                                    fullWidth
                                                    id="firstName"
                                                    label="First Name"
                                                    autoFocus
                                                    value={firstname}
                                                    onChange={(e) => setFirstname(e.target.value)}
                                                    onFocus={() => setMessage("")}
                                                />
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <TextField
                                                    variant="outlined"
                                                    required
                                                    fullWidth
                                                    id="lastName"
                                                    label="Last Name"
                                                    name="lastName"
                                                    autoComplete="lname"
                                                    value={lastname}
                                                    onChange={(e) => setLastname(e.target.value)}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    variant="outlined"
                                                    required
                                                    fullWidth
                                                    id="email"
                                                    label="Email Address"
                                                    name="email"
                                                    type="email"
                                                    autoComplete="email"
                                                    value={email}
                                                    onChange={(e) => setEmail(e.target.value)}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    variant="outlined"
                                                    required
                                                    fullWidth
                                                    name="password"
                                                    label="Password"
                                                    type="password"
                                                    id="password"
                                                    autoComplete="current-password"
                                                    value={password}
                                                    onChange={(e) => setPassword(e.target.value)}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    variant="outlined"
                                                    required
                                                    fullWidth
                                                    name="Confirm Password"
                                                    label="Confirm Password"
                                                    type="password"
                                                    id="password"
                                                    autoComplete="current-password"
                                                    value={passwordConfirm}
                                                    onChange={(e) => setPasswordConfirm(e.target.value)}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                {message && <div className="message message--error">{message}</div>}
                                            </Grid>
                                        </Grid>
                                        <Button
                                            type="submit"
                                            fullWidth
                                            variant="contained"
                                            color="primary"
                                            className={classes.submit}>
                                            Sign Up
                                    </Button>
                                        <Grid container justify="flex-end">
                                            <Grid item>
                                                <Link onClick={() => window.location.pathname = "/signin"} variant="body2">
                                                    Already have an account? Sign in
                                            </Link>
                                            </Grid>
                                        </Grid>
                                    </form>
                                </Card>
                            </Container>
                        </>
                    )
            }
        ></Route>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 50,
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const styles = {
    paperContainer: {
        backgroundImage: `url(${Image})`,
        backgroundSize: "cover",
        position: 'absolute',
        width: '100%',
        height: '91%',
    }
};

const ImgBackground = styled(Paper)`
    z-index: -1;
    filter: blur(15px) brightness(0.7)
  `