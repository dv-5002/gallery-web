import React, { useState, useEffect } from "react";
import { useParams, useHistory, useRouteMatch } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import API from "../api";
import { Typography, Grid, Box, Container, Paper, Link, Checkbox, FormControlLabel, TextField, CssBaseline, Button, Avatar, Input, Fab, GridList, ListSubheader, GridListTile, IconButton, GridListTileBar } from '@material-ui/core'
import AddIcon from "@material-ui/icons/Add";
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles'
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import ResponsiveGallery from 'react-responsive-gallery'

export default function Gallery() {
    const { path } = useRouteMatch();
    const { id } = useParams();
    const history = useHistory();
    const [isNameEditing, setIsNameEditing] = useState(false);
    const [gallery, setGallery] = useState({
        id: null,
        name: "",
        is_publish: false,
    });
    const [images, setImages] = useState([]);
    const classes = useStyles()

    useEffect(() => {
        if (id !== "new") {
            const galleryID = parseInt(id, 10);
            API.getGallery(galleryID)
                .then((res) => {
                    const { id, name, is_publish } = res.data;
                    setGallery({ id, name, is_publish });
                })
                .catch((err) => console.error(err));
        }
    }, [id]);

    useEffect(() => {
        if (id !== "new") {
            API.listGalleryImage(id)
                .then((res) => {
                    setImages(res.data);
                })
                .catch((err) => console.error(err));
        }
    }, [id]);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (gallery.name.trim() === "") {
            return;
        }
        if (id === "new") {
            API.createGallery(gallery)
                .then((res) => {
                    history.replace(path.replace(":id", res.data.id));
                    const { id, name, is_publish } = res.data;
                    setGallery({ id, name, is_publish });
                    setIsNameEditing(false);
                })
                .catch((err) => console.error(err));
        } else {
            const { id, name } = gallery;
            API.updateGalleryName({ id, name })
                .then(() => {
                    setGallery({ ...gallery, name });
                })
                .catch((err) => console.error(err));
        }
    };

    const handleFileChange = (e) => {
        const form = new FormData();
        const files = e.target.files;
        for (let i = 0; i < files.length; i++) {
            form.append("photos", files[i]);
        }
        API.upload(id, form)
            .then((res) => {
                setImages([...images, ...res.data]);
            })
            .catch((e) => {
                console.error(e);
            });
    };

    const handleDelete = (id) => {
        API.deleteImage(id)
            .then(() => {
                const filter = images.filter((img) => img.id !== id);
                setImages(filter);
            })
            .catch((e) => {
                console.error(e);
            });
    };

    return (
        <Container>
            <Box mt={8}>
                <form className="gallery-form" onSubmit={handleSubmit}>
                    <Grid container justify={'flex-end'}>
                        <Button
                            size={'large'}
                            variant={'contained'}
                            color='primary'
                            type="submit"
                            style={{ marginLeft: 8 }}
                            onClick={() => window.location.pathname = "/gallery/my"}>Done</Button>
                    </Grid>
                    <Box mb={8} style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <TextField
                            variant="outlined"
                            type="text"
                            value={gallery.name}
                            placeholder="Gallery's Name"
                            onFocus={() => setIsNameEditing(true)}
                            onBlur={() => setIsNameEditing(false)}
                            onChange={(e) => setGallery({ ...gallery, name: e.target.value })}
                            autoFocus />
                        <label htmlFor="photos">
                            <Input
                                style={{ display: 'none' }}
                                variant="outlined"
                                type="file"
                                id="photos"
                                name="photos"
                                multiple
                                onChange={handleFileChange}
                            />
                            <Button
                                style={{ marginLeft: 8 }}
                                component="span"
                                aria-label="add"
                                size={'large'}
                                variant={'outlined'}
                                color='primary'>
                                <AddIcon /> Upload photo
                            </Button>
                        </label>
                    </Box>
                </form>
                <div className={classes.root}>
                    <GridList cellHeight={250} className={classes.gridList} cols={3}>
                        {images.map((img) => (
                            <GridListTile key={img.id}>
                                <GridListTileBar
                                    titlePosition="top"
                                    classes={{
                                        root: classes.titleBar,
                                        title: classes.title,
                                    }}
                                    actionIcon={
                                        <IconButton onClick={() => { handleDelete(img.id) }} style={{ zIndex: 1,color:'#8E0000' }}>
                                            <ClearRoundedIcon />
                                        </IconButton>
                                    } />
                                <img
                                    className={classes.image}
                                    src={`${API.host}/${img.filename}`} />
                            </GridListTile>
                        ))}
                    </GridList>
                </div>
            </Box>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-around",
        overflow: "hidden",
        marginBottom: 32,
    },
    gridList: {
        //width: 900,
        height: "auto",
    },
    icon: {
        color: "rgba(255, 255, 255, 0.54)"
    },
    image: {
        "&:hover": {
            opacity: 0.25
        }
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
            'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    }
}))