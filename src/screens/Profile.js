import React, { useState, useEffect } from "react";
import { useParams, useHistory, useRouteMatch } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import API from "../api";
import { Typography, Grid, Box, Container, Paper, Link, Checkbox, FormControlLabel, TextField, CssBaseline, Button, Avatar, Input, Fab, GridList, ListSubheader, GridListTile, IconButton, GridListTileBar, InputLabel } from '@material-ui/core'
import AddIcon from "@material-ui/icons/Add";
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles'
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import ResponsiveGallery from 'react-responsive-gallery'

export default function Profile(props) {
    const { url } = useRouteMatch();
    const { id } = useParams();
    const all = useParams();
    const history = useHistory();
    const [isFirstnameEditing, setIsFirstnameEditing] = useState(false);
    const [isLastnameEditing, setIsLastnameEditing] = useState(false);
    const [user, setUser] = useState({
        id: null,
        Firstname: "",
        Lastname: "",
    });
    const [images, setImages] = useState([]);
    const classes = useStyles()

    useEffect(() => {
        const userID = parseInt(id);
        API.getUser(userID)
            .then((res) => {
                const { ID, Firstname, Lastname } = res.data;
                console.log(res)
                setUser({ ID, Firstname, Lastname });
            })
            .catch((err) => console.error(err));
    }, [id]);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (user.Firstname.trim() === "") {
            return;
        } if (user.Lastname.trim() === "") {
            return;
        } else {
            const { ID, Firstname, Lastname } = user;
            API.updateProfile({ ID, Firstname, Lastname })
                .then(() => {
                    setUser({ ...user, Firstname, Lastname });
                })
                .catch((err) => console.error(err));
        }
    };

    return (
        <Container>
            <Box mt={8}>
                <form className="gallery-form" onSubmit={handleSubmit}>
                    <Grid container justify={'flex-end'}>
                        <Button
                            size={'large'}
                            variant={'contained'}
                            color='primary'
                            type="submit"
                            style={{ marginLeft: 8 }}
                        // onClick={() => window.location.pathname = "/gallery/my"}
                        >Done</Button>
                    </Grid>
                    <Box mb={8} style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <TextField
                            variant="outlined"
                            type="text"
                            value={user.Firstname}
                            placeholder="Firstname Name"
                            onFocus={() => setIsFirstnameEditing(true)}
                            onBlur={() => setIsFirstnameEditing(false)}
                            onChange={(e) => setUser({ ...user, Firstname: e.target.value })}
                            autoFocus />
                        <TextField
                            style={{ marginLeft: 8 }}
                            variant="outlined"
                            type="text"
                            value={user.Lastname}
                            placeholder="Lastname Name"
                            onFocus={() => setIsLastnameEditing(true)}
                            onBlur={() => setIsLastnameEditing(false)}
                            onChange={(e) => setUser({ ...user, Lastname: e.target.value })}
                            autoFocus />
                    </Box>
                </form>
            </Box>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-around",
        overflow: "hidden",
        marginBottom: 32,
    },
    gridList: {
        //width: 900,
        height: "auto",
    },
    icon: {
        color: "rgba(255, 255, 255, 0.54)"
    },
    image: {
        "&:hover": {
            opacity: 0.25
        }
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
            'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    }
}))